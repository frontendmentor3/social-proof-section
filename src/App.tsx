import PersonCard from "./components/PersonCard";
import StarReview from "./components/StarReview";
import styled from "styled-components";
import bgBottomDesktop from './images/bg-pattern-bottom-desktop.svg'
import bgBottomMobile from './images/bg-pattern-bottom-mobile.svg'
import bgTopDesktop from './images/bg-pattern-top-desktop.svg'
import bgTopMobile from './images/bg-pattern-top-mobile.svg'
import {devices} from "./styles/sizes";

const testimonials = [
    {
        image:"image-colton.jpg",
        name: "Colton Smith",
        subtitle: "Verified Buyer",
        content: "We needed the same printed design as the one we had ordered a week prior. + " +
            "Not only did they find the original order, but we also received it in time. Excellent!"
    },
    {
        image:"image-irene.jpg",
        name: "Irene Roberts",
        subtitle: "Verified Buyer",
        content: "Customer service is always excellent and very quick turn around. Completely\n" +
            "delighted with the simplicity of the purchase and the speed of delivery."
    },
    {
        image:"image-anne.jpg",
        name: "Anne Wallace",
        subtitle: "Verified Buyer",
        content: "Put an order with this company and can only praise them for the very high " +
            "standard. Will definitely use them again and recommend them to everyone!"
    }
]

const starReviews = [
    {
        star: 5,
        text: "Rated 5 Stars in Reviews"
    },
    {
        star: 5,
        text: "Rated 5 Stars in Report Guru"
    },
    {
        star: 5,
        text: "Rated 5 Stars in BestTech"
    }
]

const ContainerStyles = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
  justify-items: center;  
  background-image: url(${bgTopMobile}), url(${bgBottomMobile});
  background-repeat: no-repeat, no-repeat;
  background-position: top, bottom;
  
  @media ${devices.desktop}{
    padding: 100px;
    display: grid;
    max-width: 1300px;
    margin: auto;
    grid-template-areas: "heading reviews"
                        "testimonials testimonials"
                        "attribute attribute";
    grid-template-columns: 1fr 2fr;
    background-image: url(${bgTopDesktop}), url(${bgBottomDesktop});
    background-repeat: no-repeat, no-repeat;
    background-position: top, bottom;
  }
`

const HeadingTextContainerStyles = styled.div`
    grid-area: heading;
`

const StarReviewContainerStyles = styled.div`
  grid-area: reviews;
`

const TestimonialContainerStyles = styled.div`
    grid-area: testimonials;
    @media ${devices.desktop}{
      display: flex;
      gap: 30px;
      flex-direction: row;      
    }
`

const HeadingStyles = styled.h1`
  color: var(--very-dark-magenta);
  text-align: center;
  @media ${devices.desktop}{
    text-align: left;
  }
`

const BasicTextStyles = styled.div`
  color: var(--dark-grayish-magenta);
  text-align: center;
  font-size: 0.9rem;
  padding: 30px 0;
  @media ${devices.desktop}{
    text-align: left;
    padding: 0px;
  }
`

const AttributionStyles = styled.div`
  grid-area: attribute;  
  margin-top: 60px;
  font-size: 11px;
  text-align: center;
  
  a {
    color: hsl(228, 45%, 44%);
  }
`

function App() {
    return (
        <ContainerStyles>
            <HeadingTextContainerStyles>
                <HeadingStyles>10,000+ of our users love our products.</HeadingStyles>
                <BasicTextStyles>
                    We only provide great products combined with excellent customer service.
                    See what our satisfied customers are saying about our services.
                </BasicTextStyles>
            </HeadingTextContainerStyles>
            <StarReviewContainerStyles>
                {starReviews.map(r=>(
                    <StarReview star={r.star} text={r.text} />
                ))}
            </StarReviewContainerStyles>
            <TestimonialContainerStyles>
                {testimonials.map(t => (
                    <PersonCard image={t.image} name={t.name} subtitle={t.subtitle} content={t.content}/>
                ))}
            </TestimonialContainerStyles>
            <AttributionStyles className="attribution">
                Challenge by
                <a href="https://www.frontendmentor.io?ref=challenge" rel="noopener noreferrer" target="_blank"
                >Frontend Mentor</a
                >. Coded by <a href="https://github.com/cherylli">CM</a>.
            </AttributionStyles>
        </ContainerStyles>
    );
}

export default App;
