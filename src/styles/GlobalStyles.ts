import {createGlobalStyle} from 'styled-components';
import {fontSize} from "./sizes";


export const GlobalStyles = createGlobalStyle`
  :root{
    --very-dark-magenta: hsl(300, 43%, 22%);
    --soft-pink: hsl(333, 80%, 67%);
    
    --dark-grayish-magenta: hsl(303, 10%, 53%);
    --light-grayish-magenta: hsl(300, 24%, 96%);
    --white: hsl(0, 0%, 100%);
    
    font-family: 'Spartan', sans-serif;
    font-size: ${fontSize.primary};
  }
`