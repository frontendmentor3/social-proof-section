import styled from "styled-components";
import {devices} from "../styles/sizes";

type PersonCardProps = {
    image: string;
    name: string;
    subtitle: string;
    content: string;
}

const ContainerStyles = styled.div`
  background-color: var(--very-dark-magenta);
  border-radius: 20px;
  padding: 10px;
  display: flex;
  flex-direction: column;
  margin: 20px 0px;
  padding: 30px;
  gap: 30px;

  @media ${devices.desktop} {
    &:nth-child(2){
      position: relative;
      top: 20px;
    }
    &:nth-child(3){
      position: relative;
      top: 40px;
    }
  }
`
const PersonHeaderStyles = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 20px;
`

const PersonImageStyles = styled.img`
  display: flex;
  flex-direction: column;  
  border-radius: 9999px;
  width:40px;
`

const PersonNameStyles = styled.div`
  .person__name{
    color: var(--white);
    font-weight: bold;
    
  }
  .person_subtitle{
    color: var(--soft-pink);
    font-size: 0.8rem;
  }
`

const PersonContentStyles = styled.div`
  color: var(--light-grayish-magenta);
  font-size: 0.8rem;
`

const PersonCard = ({image, name, subtitle, content }: PersonCardProps) => <ContainerStyles>
    <PersonHeaderStyles>
        <PersonImageStyles src={`/assets/images/${image}`} />
        <PersonNameStyles>
            <div className="person__name">{name}</div>
            <div className="person_subtitle">{subtitle}</div>
        </PersonNameStyles>

    </PersonHeaderStyles>
    <PersonContentStyles>
        {`"${content}"`}
    </PersonContentStyles>
</ContainerStyles>

export default PersonCard