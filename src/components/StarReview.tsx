import styled from "styled-components";
import iconStar from '../images/icon-star.svg'
import {devices} from "../styles/sizes";

type StarReviewProps = {
    star: number;
    text: string;
}

const ContainerStyles = styled.div`
  background-color: var(--light-grayish-magenta);
  padding: 20px 10px;
  margin: 20px 0px;
  min-width: 250px;
  border-radius: 10px;
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 15px;
  @media ${devices.desktop}{
    flex-direction: row;
    width: 320px;
    padding-left: 30px;
    
    &:nth-child(2){
      position: relative;
      right: -60px;
      
    }
    &:nth-child(3){
      position: relative;
      right: -120px;
    }
  }
`

const StarStyles = styled.div`
  display: flex;
  gap: 5px;
`

const TextStyles = styled.div`
  font-weight: bold;
  font-size: 0.7rem;
  color: var(--very-dark-magenta);
  
`

const StarReview = ({star, text }: StarReviewProps) => (<ContainerStyles>
    <StarStyles>
        {[...Array(star)].map(()=>(
            <img alt="star" src={iconStar}/>
        ))}
    </StarStyles>
    <TextStyles>{text}</TextStyles>
</ContainerStyles>)

export default StarReview